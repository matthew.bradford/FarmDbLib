extern crate ansi_term;
extern crate chrono;
extern crate configuration;

#[macro_use]
extern crate diesel;
extern crate dotenv;
#[macro_use]
extern crate diesel_codegen;
extern crate r2d2;
extern crate r2d2_diesel;
#[macro_use]
extern crate lazy_static;

use diesel::pg::PgConnection;
use r2d2_diesel::ConnectionManager;

pub mod accounting_models;
pub mod bee_models;
pub mod farm_models;
pub mod plant_models;

lazy_static! {
        static ref CON_POOL: r2d2::Pool<r2d2_diesel::ConnectionManager<diesel::pg::PgConnection>> = ConPool::new();
}

pub struct ConPool {
    pub pool: r2d2::Pool<r2d2_diesel::ConnectionManager<diesel::pg::PgConnection>>,
}

impl ConPool {
    pub fn new() -> r2d2::Pool<r2d2_diesel::ConnectionManager<diesel::pg::PgConnection>> {
        let config = r2d2::Config::builder()
            .pool_size(configuration::get_value("pgcon_count").parse::<u32>().unwrap())//
            .build();
        let manager = ConnectionManager::<PgConnection>::new(configuration::get_value("pgcon"));
        r2d2::Pool::new(config, manager).unwrap()
    }

    pub fn get_connection() -> r2d2::Pool<ConnectionManager<PgConnection>> {
        CON_POOL.clone()
    }
}

#[cfg(test)]
mod tests {
    #[test]
    fn it_works() {}
}
