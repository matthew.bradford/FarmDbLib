use diesel;
use diesel::prelude::*;
use ConPool;

use accounting_models::schema::accounting::general_ledger_records;

use chrono::{ Datelike, NaiveDate };

#[derive(Insertable)]
#[table_name = "general_ledger_records"]
pub struct NewGLRecord {
    journal_id: i32,
    reversal_ind: bool,
    reversal_journal_id: Option<i32>,
    business_unit_id: i32,
    fiscal_year: i32,
    period: i32,
    effective_date: i32,
    entry_date: i32,
    user_id: i32,
    gl_account_id: i32,
    amount: i64,
    amount_currency_type_id: i32,
    reporting_amount: i64,
    reporting_amount_currency_type_id: i32,
    local_amount: i64,
    local_amount_currency_type_id: i32,
    dr_cr_ind: String,
}

impl NewGLRecord {
    pub fn new(journal_id: i32, reversal_ind: bool, reversal_journal_ref: Option<i32>, business_unit_id: i32,
    fiscal_year: i32, period: i32, effective_date: NaiveDate, entry_date: NaiveDate, user_id: i32, gl_account_id: i32,
    amount: i64, amount_curency_type_id: i32, reporting_amount: i64, reporting_amount_currency_type_id: i32,
    local_amount: i64, local_amount_currency_type_id: i32, dr_cr_ind: &str) -> NewGLRecord {
        NewGLRecord {
            journal_id: journal_id,
            reversal_ind: reversal_ind,
            reversal_journal_id: reversal_journal_ref,
            business_unit_id: business_unit_id,
            fiscal_year: fiscal_year,
            period: period,
            effective_date: effective_date.num_days_from_ce(),
            entry_date: entry_date.num_days_from_ce(),
            user_id: user_id,
            gl_account_id: gl_account_id,
            amount: amount,
            amount_currency_type_id: amount_curency_type_id,
            reporting_amount: reporting_amount,
            reporting_amount_currency_type_id: reporting_amount_currency_type_id,
            local_amount: local_amount,
            local_amount_currency_type_id: local_amount_currency_type_id,
            dr_cr_ind: dr_cr_ind.to_string(),
        }
    }
}

#[allow(dead_code)]
#[derive(Queryable, Identifiable, PartialEq, AsChangeset)]
pub struct GeneralLedgerRecord {
    id: i32,
    journal_id: i32,
    reversal_ind: bool,
    reversal_journal_id: Option<i32>,
    business_unit_id: i32,
    fiscal_year: i32,
    period: i32,
    effective_date: i32,
    entry_date: i32,
    user_id: i32,
    gl_account_id: i32,
    amount: i64,
    amount_currency_type_id: i32,
    reporting_amount: i64,
    reporting_amount_currency_type_id: i32,
    local_amount: i64,
    local_amount_currency_type_id: i32,
    dr_cr_ind: String,
}

impl GeneralLedgerRecord {
    pub fn new(id: i32, journal_id: i32, reversal_ind: bool, reversal_journal_ref: Option<i32>, business_unit_id: i32,
               fiscal_year: i32, period: i32, effective_date: NaiveDate, entry_date: NaiveDate, user_id: i32, gl_account_id: i32,
               amount: i64, amount_curency_type_id: i32, reporting_amount: i64, reporting_amount_currency_type_id: i32,
               local_amount: i64, local_amount_currency_type_id: i32, dr_cr_ind: &str) -> GeneralLedgerRecord {
        GeneralLedgerRecord {
            id: id,
            journal_id: journal_id,
            reversal_ind: reversal_ind,
            reversal_journal_id: reversal_journal_ref,
            business_unit_id: business_unit_id,
            fiscal_year: fiscal_year,
            period: period,
            effective_date: effective_date.num_days_from_ce(),
            entry_date: entry_date.num_days_from_ce(),
            user_id: user_id,
            gl_account_id: gl_account_id,
            amount: amount,
            amount_currency_type_id: amount_curency_type_id,
            reporting_amount: reporting_amount,
            reporting_amount_currency_type_id: reporting_amount_currency_type_id,
            local_amount: local_amount,
            local_amount_currency_type_id: local_amount_currency_type_id,
            dr_cr_ind: dr_cr_ind.to_string(),
        }
    }

    pub fn get_id(&self) -> i32 {
        self.id
    }

    pub fn set_id(&mut self, id: i32) {
        self.id = id;
    }

    pub fn get_journal_id(&self) -> i32 {
        self.journal_id
    }

    pub fn set_journal_id(&mut self, journal_id: i32) {
        self.journal_id = journal_id;
    }

    pub fn get_reversal_ind(&self) -> bool {
        self.reversal_ind
    }

    pub fn set_reversal_ind(&mut self, reversal_ind: bool){
        self.reversal_ind = reversal_ind;
    }

    pub fn get_reversal_journal_id(&self) -> Option<i32> {
        self.reversal_journal_id
    }

    pub fn set_reversal_journal_id(&mut self, reversal_journal_id: i32) {
        self.reversal_journal_id = Some(reversal_journal_id);
    }

    pub fn get_business_unit_id(&self) -> i32 {
        self.business_unit_id
    }

    pub fn set_business_unit_id(&mut self, business_unit_id: i32) {
        self.business_unit_id = business_unit_id;
    }

    pub fn get_fiscal_year (&self) -> i32 {
        self.fiscal_year
    }

    pub fn set_fiscal_year (&mut self, fiscal_year: i32) {
        self.fiscal_year = fiscal_year;
    }

    pub fn get_period(&self) -> i32 {
        self.period
    }

    pub fn set_period(&mut self, period: i32) {
        self.period = period;
    }

    pub fn get_effective_date(&self) -> NaiveDate {
        NaiveDate::from_num_days_from_ce_opt(self.effective_date).unwrap()
    }

    pub fn set_effective_date(&mut self, date: NaiveDate) {
        self.effective_date = date.num_days_from_ce();
    }

    pub fn get_entry_date(&self) -> NaiveDate {
        NaiveDate::from_num_days_from_ce_opt(self.entry_date).unwrap()
    }

    pub fn set_entry_date(&mut self, date: NaiveDate) {
        self.entry_date = date.num_days_from_ce();
    }

    pub fn get_user_id(&self) -> i32 {
        self.user_id
    }

    pub fn set_user_id(&mut self, user_id: i32) {
        self.user_id = user_id;
    }

    pub fn get_gl_account(&self) -> i32 {
        self.gl_account_id
    }

    pub fn set_gl_account(&mut self, gl_account_id: i32) {
        self.gl_account_id = gl_account_id;
    }

    pub fn get_amount(&self) -> i64 {
        i64::from(self.amount)
    }

    pub fn set_amount(&mut self, amount: i64) {
        self.amount = amount;
    }

    pub fn get_amount_currency_type_id(&self)  -> i32 {
        self.amount_currency_type_id
    }

    pub fn set_amount_currency_type_id(&mut self, currenc_type_id: i32) {
        self.amount_currency_type_id = currenc_type_id;
    }

    pub fn get_reporting_amount(&self) -> i64 {
        self.reporting_amount
    }

    pub fn set_reporting_amount(&mut self, reporting_amount: i64) {
        self.reporting_amount = reporting_amount;
    }

    pub fn get_reporting_amount_currency_id(&self) -> i32 {
        self.reporting_amount_currency_type_id
    }

    pub fn set_reporting_currency_type_id(&mut self, currency_type_id: i32) {
        self.reporting_amount_currency_type_id = currency_type_id;
    }

    pub fn get_local_amount(&self) -> i64 {
        self.local_amount
    }

    pub fn set_local_amount(&mut self, amount: i64) {
        self.local_amount = amount;
    }

    pub fn get_local_amount_currency_type_id(&self) -> i32 {
        self.local_amount_currency_type_id
    }

    pub fn set_local_amount_currency_type_id(&mut self, currency_type_id: i32) {
        self.local_amount_currency_type_id = currency_type_id;
    }

    pub fn get_debit_credit_ind(&self) -> &str {
        &self.dr_cr_ind
    }

    pub fn set_debit_credit_ind(&mut self, ind: &str) {
        self.dr_cr_ind = ind.to_string();
    }

    #[allow(unused_assignments)]
    pub fn get_general_ledger_records() -> Option<Vec<GeneralLedgerRecord>> {
        use self::general_ledger_records::dsl::*;
        let pool = ConPool::get_connection();
        let d_gl_record = match general_ledger_records.load::<GeneralLedgerRecord>(&*pool.get().unwrap()) {
            Ok(d) => Some(d),
            Err(..) => None,
        };
        d_gl_record
    }

    pub fn create_general_ledger_record<'a>(p_gl_entry: &'a NewGLRecord) -> GeneralLedgerRecord {
        let pool = ConPool::get_connection();

        diesel::insert(p_gl_entry)
            .into(general_ledger_records::table)
            .get_result(&*pool.get().unwrap())
            .expect("Error saving new general ledger record")
    }

    #[allow(unused_must_use)]
    pub fn update_general_ledger_record(p_gl_record: &GeneralLedgerRecord) {
        use self::general_ledger_records::dsl::*;
        let pool = ConPool::get_connection();

        diesel::update(general_ledger_records.filter(id.eq(p_gl_record.id)))
            .set(p_gl_record)
            .execute(&*pool.get().unwrap());
    }

}
