use diesel;
use diesel::prelude::*;
use ConPool;

use accounting_models::schema::accounting::journal_records;

/*
CREATE TABLE accounting.journal_records (
    id SERIAL NOT NULL PRIMARY KEY,
    "date" BIGINT NOT NULL,
    initial_entries_id INT NOT NULL REFERENCES accounting.initial_records(id),
    gl_account_id INT NOT NULL REFERENCES accounting.chart_of_accounts(id),
    debit BIGINT NOT NULL DEFAULT (0),
    credit BIGINT NOT NULL DEFAULT (0)
);
*/

#[derive(Insertable)]
#[table_name="journal_records"]
pub struct NewJournalRecord {
    date: i64,
    initial_entries_id: i32,
    gl_account_id: i32,
    debit: i64,
    credit: i64,
}


#[derive(Queryable, Identifiable, PartialEq, AsChangeset, Associations)]
#[belongs_to(GeneralLedgerRecord)]
pub struct JournalRecord {
    id: i32,
    date: i64,
    initial_entries_id: i32,
    gl_account_id: i32,
    debit: i64,
    credit: i64,
}

#[allow(unused_assignments)]
pub fn get_journal_records() -> Option<Vec<JournalRecord>> {
    use self::journal_records::dsl::*;
    let pool = ConPool::get_connection();
    let d_journal_records = match journal_records.load::<JournalRecord>(&*pool.get().unwrap()) {
        Ok(d) => Some(d),
        Err(..) => None,
    };
    d_journal_records
}

pub fn create_journal_record(p_journalrec: &NewJournalRecord) -> JournalRecord {
    let pool = ConPool::get_connection();

    diesel::insert(p_journalrec)
        .into(journal_records::table)
        .get_result(&*pool.get().unwrap())
        .expect("Error saving new currency type")
}

#[allow(unused_must_use, unused_assignments)]
pub fn update_journal_record(p_journal_record: &JournalRecord) {
    use self::journal_records::dsl::*;
    let pool = ConPool::get_connection();

    diesel::update(journal_records.filter(id.eq(p_journal_record.id)))
        .set(p_journal_record)
        .execute(&*pool.get().unwrap());
}

#[allow(unused_must_use)]
pub fn delete_journal_record(p_journal_record_id: i32) {
    use self::journal_records::dsl::*;
    let pool = ConPool::get_connection();
    diesel::delete(journal_records.filter(id.eq(p_journal_record_id)))
        .execute(&*pool.get().unwrap());
}