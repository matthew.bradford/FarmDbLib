pub mod account_group;
pub mod account_sub_group;
pub mod initial_record;
pub mod business_unit;
pub mod currency_type;
pub mod category_type;
pub mod general_ledger_record;
pub mod chart_of_account;
pub mod journal_record;
pub mod schema;

pub enum RecordStatus {
    NotSet = -1,
    Active = 0,
    Archived = 1,
}