use diesel;
use diesel::prelude::*;
use ConPool;

use accounting_models::schema::accounting::account_groups;

/*
CREATE TABLE IF NOT EXISTS accounting.account_categories (
    id SERIAL PRIMARY KEY NOT NULL,
    category_type_id INT NOT NULL REFERENCES accounting.category_types(id),
    name VARCHAR NOT NULL,
    dr_cr_char CHAR NOT NULL,
    status INT NOT NULL DEFAULT(0)
);*/


#[derive(Insertable)]
#[table_name="account_groups"]
pub struct NewAccountGroup {
    pub category_type_id: i32,
    pub name: String,
    pub dr_cr_char: String,
    pub status: i32,
}

#[derive(Queryable, Identifiable, PartialEq, AsChangeset, Associations)]
#[belongs_to(ChartOfAccount)]
pub struct AccountGroup {
    id: i32,
    category_type_id: i32,
    name: String,
    dr_cr_char: String,
    status: i32,
}

impl AccountGroup {
    pub fn new(p_id: i32, p_category_type_id: i32, p_name: &str, p_dr_cr_char: &str, p_status: i32) -> AccountGroup {
        AccountGroup {
            id: p_id,
            category_type_id: p_category_type_id,
            name: p_name.to_string(),
            dr_cr_char: p_dr_cr_char.to_string(),
            status: p_status,
        }
    }

    pub fn get_id(&self) -> i32 {
        self.id
    }

    pub fn get_category_type_id(&self) -> i32 {
        self.category_type_id
    }

    pub fn get_name(&self) -> &str {
        &self.name
    }

    pub fn get_dr_cr_char(&self) -> &str {
        &self.dr_cr_char
    }

    pub fn get_status(&self) -> i32 {
        self.status
    }
}

#[allow(unused_assignments)]
pub fn get_account_groups() -> Option<Vec<AccountGroup>> {
    use self::account_groups::dsl::*;
    let pool = ConPool::get_connection();
    let l_account_groups = match account_groups.load::<AccountGroup>(&*pool.get().unwrap()) {
        Ok(d) => Some(d),
        Err(..) => None,
    };
    l_account_groups
}

#[allow(unused_assignments)]
pub fn get_account_group(p_id: i32) -> Option<AccountGroup> {
    use self::account_groups::dsl::*;
    let pool = ConPool::get_connection();
    let dbrecord = match account_groups.filter(id.eq(p_id)).get_result::<AccountGroup>(&*pool.get().unwrap()) {
        Ok(d) => Some(d),
        Err(..) => None,
    };
    dbrecord
}

pub fn create_account_group(p_account_group: &NewAccountGroup) -> AccountGroup {
    let pool = ConPool::get_connection();

    diesel::insert(p_account_group)
        .into(account_groups::table)
        .get_result(&*pool.get().unwrap())
        .expect("Error saving account group")
}

#[allow(unused_must_use, unused_assignments)]
pub fn update_account_group(p_account_group: &AccountGroup) {
    use self::account_groups::dsl::*;
    let pool = ConPool::get_connection();

    diesel::update(account_groups.filter(id.eq(p_account_group.id)))
        .set(p_account_group)
        .execute(&*pool.get().unwrap());
}

#[allow(unused_must_use)]
pub fn delete_category_type(p_account_group_id: i32) {
    use self::account_groups::dsl::*;
    let pool = ConPool::get_connection();
    diesel::delete(account_groups.filter(id.eq(p_account_group_id)))
        .execute(&*pool.get().unwrap());
}

