-- This file should undo anything in `up.sql`
DO $$
BEGIN
    IF EXISTS (SELECT 1 FROM information_schema.tables WHERE  table_schema = 'plant' AND table_name = 'plants') THEN
        CREATE TABLE IF NOT EXISTS public.tmp_plants AS (SELECT * FROM plant.plants);
    END IF;
END $$;
DO $$
BEGIN
    IF EXISTS (SELECT 1 FROM information_schema.tables WHERE table_schema = 'plant' AND table_name = 'common_names') THEN
        CREATE TABLE IF NOT EXISTS public.tmp_common_names AS (SELECT * FROM plant.common_names);
    END IF;
END $$;

DROP TABLE plant.common_names;
DROP TABLE plant.plants;

DROP SCHEMA plant CASCADE;