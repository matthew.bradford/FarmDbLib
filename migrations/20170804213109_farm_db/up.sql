
CREATE SCHEMA IF NOT EXISTS plant;

CREATE TABLE IF NOT EXISTS plant.plants (
    id SERIAL PRIMARY KEY,
    family TEXT NOT NULL,
    genus TEXT NOT NULL,
    species TEXT NOT NULL,
    variety TEXT NOT NULL
);

DO $$
BEGIN
    IF EXISTS(SELECT 1 from information_schema.tables WHERE  table_schema = 'public' AND table_name = 'tmp_plants') THEN
        INSERT INTO plant.plants
        SELECT * FROM public.tmp_plants ORDER BY id ASC;

        PERFORM setval('plant.plants_id_seq', COALESCE((SELECT MAX(id)+1 FROM plant.plants), 1), false);

        DROP TABLE public.tmp_plants;
    END IF;
END $$;

CREATE TABLE IF NOT EXISTS plant.common_names (
    id SERIAL NOT NULL PRIMARY KEY,
    plant_id INT NOT NULL REFERENCES plant.plants(id),
    name TEXT NOT NULL
);

DO $$
BEGIN
    IF EXISTS(SELECT 1 from information_schema.tables WHERE  table_schema = 'public' AND table_name = 'tmp_common_names') THEN
        INSERT INTO plant.common_names
            SELECT * FROM public.tmp_common_names ORDER BY id ASC;

        PERFORM setval('plant.common_names_id_seq', COALESCE((SELECT MAX(id)+1 FROM plant.common_names), 1), false);

        DROP TABLE public.tmp_common_names;
    END IF;
END $$;