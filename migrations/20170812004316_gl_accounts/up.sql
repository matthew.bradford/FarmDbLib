-- Your SQL goes here

CREATE TABLE accounting.journal_records (
    id SERIAL NOT NULL PRIMARY KEY,
    "date" BIGINT NOT NULL,
    initial_entries_id INT NOT NULL REFERENCES accounting.initial_records(id),
    gl_account_id INT NOT NULL REFERENCES accounting.chart_of_accounts(id),
    debit BIGINT NOT NULL DEFAULT (0),
    credit BIGINT NOT NULL DEFAULT (0)
);

DO $$
BEGIN
    IF EXISTS (SELECT 1 FROM information_schema.tables WHERE table_schema = 'public' AND table_name = 'tmp_journal_records') THEN
        INSERT INTO accounting.journal_records("date", initial_entries_id, gl_account_id, debit, credit)
            SELECT "date", initial_entries_id, gl_account_id, debit, credit FROM public.tmp_journal_records ORDER BY "date" ASC;

        PERFORM setval('accounting.journal_records_id_seq', COALESCE((SELECT MAX(id)+1 FROM accounting.journal_records), 1), false);

        DROP TABLE public.tmp_journal_records;
    END IF;
END $$;

CREATE TABLE accounting.general_ledger_records (
    id SERIAL NOT NULL PRIMARY KEY,
    journal_id INT NOT NULL REFERENCES accounting.journal_records(id),
    reversal_ind bool NOT NULL DEFAULT(false),
    reversal_journal_id INT REFERENCES accounting.journal_records(id),
    business_unit_id INT NOT NULL REFERENCES accounting.business_units(id),
    fiscal_year INT NOT NULL,
    period INT NOT NULL,
    effective_date INT NOT NULL,
    entry_date INT NOT NULL,
    user_id INT NOT NULL, --reference user table later
    gl_account_id INT NOT NULL REFERENCES accounting.chart_of_accounts(id),
    amount BIGINT NOT NULL,
    amount_currency_type_id INT NOT NULL REFERENCES accounting.currency_types(id),
    reporting_amount BIGINT NOT NULL,
    reporting_amount_currency_type_id INT NOT NULL REFERENCES accounting.currency_types(id),
    local_amount BIGINT NOT NULL,
    local_amount_currency_type_id INT NOT NULL REFERENCES accounting.currency_types(id),
    dr_cr_ind CHAR NOT NULL
);

DO $$
BEGIN
    IF EXISTS (SELECT 1 FROM information_schema.tables WHERE table_schema = 'public' AND table_name = 'tmp_general_ledger_records') THEN
        INSERT INTO accounting.general_ledger_records
            SELECT * FROM public.tmp_general_ledger_records ORDER BY id ASC;

        PERFORM setval('accounting.general_ledger_records_id_seq', COALESCE((SELECT MAX(id)+1 FROM accounting.general_ledger_records), 1), false);

        DROP TABLE public.tmp_general_ledger_records;
    END IF;
END $$;